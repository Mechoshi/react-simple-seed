# React Simple Seed #

A simple React seed with Router 4, Redux, Lazy loading, Async/Await and Webpack.

## Technologies ##

* React 16+
* React router 4+
* Redux
* React-thunk
* ES6
* Async/Await
* SCSS
* Webpack 3
* Git Flow

## To start development ##

1. Clone the repository.
2. Navigate to the root project folder.
3. Open console.
4. Type and run the following command:
```
npm install
```
5. Next, to run the development server type and run the following command:
```
npm run develop
```
6. Now `Webpack` will rebuild the project on saving changes in the `js, jsx, html, css, scss` files.
7. To open the app in the browser, open a browser window and navigate to `localhost:8000`

## To build for production ##

1. Clone the repository.
2. Navigate to the root project folder.
3. Open console.
4. Type and run the following command:
```
npm install
```
5. Next, type and run the following command:
```
npm run production
```
6. Now `Webpack` will build the project files and put them in the `dist/` folder.
7. To test the build you can start a simple prod server by typing and running the following command in the console:
```
npm run server:prod
```
8. To open the app in the browser, open a browser window and navigate to `localhost:8001`
