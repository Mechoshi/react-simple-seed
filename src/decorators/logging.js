export default function logging({background, color}) {
    return function (target) {
        target.prototype.log = function (data) {
            console.log(`%c ${data}`, `padding: 3px; background: ${background}; color: ${color};`);
        };
    };
}