import {render} from 'react-dom';

import './../styles/main';
import createApp from './createApp';

render(createApp(), document.getElementById('app'));
