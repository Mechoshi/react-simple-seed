import React from 'react';
import {Provider} from 'react-redux';
import {applyMiddleware, compose, createStore} from 'redux';
import thunk from 'redux-thunk';

import App from './App';
import rootReducerFactory from './store/reducers';

const createApp = () => {
    const rootReducer = rootReducerFactory();
    const composeEnhancers = process.env.NODE_ENV === 'production'
        ? compose
        : (window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose);
    const store = createStore(rootReducer, composeEnhancers(applyMiddleware(thunk)));
    return (<Provider store={store}><App/></Provider>);
};

export default createApp;


