export const copy = obj => {
    try {
        return JSON.parse(JSON.stringify(obj));
    } catch (error) {
        console.error(error);
        return obj;
    }
};

export const recursiveCopy = o => {
    // If primitive, null or undefined:
    if (typeof o !== 'object' || !o) {
        return o;
    }
    // If array:
    if (Array.isArray(o)) {
        return o
            .reduce((res, item) => {
                res.push(recursiveCopy(item));
                return res;
            }, []);
    }
    // If object:
    return Object.keys(o)
        .reduce((res, key) => {
            res[key] = recursiveCopy(o[key]);
            return res;
        }, {});
};

export const getRef = (obj, path) => {
    path = transformPath(path);
    let ref = obj;
    for (let i = 0; i < path.length - 1; i++) {
        ref = ref[path[i]];
    }
    return ref;
};

export const getFullRef = (obj, path) => {
    path = transformPath(path);
    let ref = obj;
    for (let i = 0; i < path.length; i++) {
        ref = ref[path[i]];
    }
    return ref;
};

export const transformPath = path => {
    let transformedPath = path;
    if (typeof path === 'string') {
        transformedPath = path.split('.');
    }
    return transformedPath;
};

export const setKey = (obj, path, val) => {
    path = transformPath(path);
    let propToSet = getRef(obj, path);
    propToSet[path[path.length - 1]] = val;
};
