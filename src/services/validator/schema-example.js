const getPetSchema = () => {
    return {
        name: {
            type: 'string',
            content: null,
            rules: {
                minLength: {
                    val: 5,
                    msg: 'Name must be at least 5 symbols long'
                }
            },
            errors: null
        },
        age: {
            type: 'number',
            content: null,
            rules: {
                minValue: 5
            },
            errors: null
        }
    }
};

const getFriendSchema = () => {
    return {
        $$$: { // !!! nb: $$$ for primitive value (not a key/value pair)
            type: 'string',
            content: null,
            rules: {
                minLength: {
                    val: 5,
                    msg: 'Friend name should be at least 5 symbols'
                }
            },
            errors: null
        }
    }
};

const getUserSchema = () => {
    return {
        firstName: {
            type: 'string',
            content: null,
            rules: {
                minLength: 3,
                maxLength: {
                    val: 10
                }
            },
            errors: null
        },
        age: {
            type: 'number',
            content: null,
            rules: {
                minValue: 16,
                maxValue: 21
            },
            errors: null
        },
        friends: {
            type: 'array',
            getSchema: getFriendSchema,
            content: null,
            rules: {
                minLength: {
                    val: 3,
                    msg: 'Select at least 3 friends'
                }
            },
            errors: null
        },
        pet: {
            type: 'object',
            getSchema: getPetSchema,
            content: null,
            rules: {
                required: true
            },
            errors: null
        }
    }
};

export const getUsersSchema = () => {
    return {
        users: {
            type: 'array',
            getSchema: getUserSchema,
            content: null,
            rules: {
                minLength: {
                    val: 3,
                    msg: 'Users must be at least 3'
                }
            },
            errors: null
        }
    }
};




