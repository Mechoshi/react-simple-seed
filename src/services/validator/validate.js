import validators from './validators';

// Pass the rootItem in order to handle cases
// like passwords equality by referencing other 
// root item's values:
export const validate = (item, schema, itemIsPrimitiveArrayElement, rootItem) => {
    if (itemIsPrimitiveArrayElement) {
        schema.rules
        && typeof schema.rules === 'object'
        && Object.keys(schema.rules)
            .forEach(r => {
                // Set the predicate and the message
                // according to the rule's structure:
                let v;
                let m;
                if (schema.rules[r] && typeof schema.rules[r] === 'object') {
                    v = schema.rules[r].val;
                    m = schema.rules[r].msg;
                } else {
                    v = schema.rules[r];
                }
                // Here the actual validation happens:
                // Pass the rootItem in order to handle cases
                // like passwords equality by referencing other
                // root item's values:
                const e = validators[r](item, v, m, rootItem);
                if (e) {
                    if (schema.errors === null) {
                        schema.errors = [];
                    }
                    schema.errors.push(e);
                }
            });
    } else {
        // For each key in the schema:
        Object.keys(schema)
            .forEach(k => {
                // Validate the current item's property:
                schema[k].rules
                && typeof schema[k].rules === 'object'
                && Object.keys(schema[k].rules)
                    .forEach(r => {
                        // Set the predicate and the message
                        // according to the rule's structure:
                        let v;
                        let m;
                        if (schema[k].rules[r] && typeof schema[k].rules[r] === 'object') {
                            v = schema[k].rules[r].val;
                            m = schema[k].rules[r].msg;
                        } else {
                            v = schema[k].rules[r];
                        }
                        // !!! NB: Determine the value to test depending on
                        // if it is an primitive (the key in the schema is $$$ or array
                        const valToTest = k === '$$$' || Array.isArray(item) ? item : item[k];
                        // Here the actual validation happens:
                        // Pass the rootItem in order to handle cases
                        // like passwords equality by referencing other
                        // root item's values:
                        const e = validators[r](valToTest, v, m, rootItem);
                        if (e) {
                            if (schema[k].errors === null) {
                                schema[k].errors = [];
                            }
                            schema[k].errors.push(e);
                        }
                    });
                // Object.keys end

                // Additional validation of the content for objects and arrays:
                switch (schema[k].type) {

                    // item's value is an OBJECT:
                    case 'object':
                        // If this item's key is actually an object: 
                        if (item[k] && typeof item[k] === 'object') {
                            // Set the content schema:    
                            if (!schema[k].content) {
                                schema[k].content = schema[k].getSchema();
                            }
                            // Pass the corresponding object for validation of it's fields:
                            // Pass the rootItem in order to handle cases
                            // like passwords equality by referencing other 
                            // root item's values:
                            validate(item[k], schema[k].content, false, rootItem);
                        }
                        break;
                    // if end

                    // item's value is an ARRAY:
                    case 'array':
                        // Determine the value to test:
                        let arrayToTest;
                        if (item && Array.isArray(item) && item.length) {
                            arrayToTest = item;
                        } else if (item[k] && Array.isArray(item[k]) && item[k].length) {
                            arrayToTest = item[k];
                        }
                        // If this item's key is actually an array and has items for validation: 
                        if (arrayToTest) {
                            arrayToTest
                                .forEach(el => {
                                    // Initialise the content as an array if not set:
                                    if (!schema[k].content) {
                                        schema[k].content = [];
                                    }
                                    // Set the array's content schema:
                                    let currentSchema = schema[k].getSchema();
                                    // Check if the schema is for a primitive:
                                    if (currentSchema.$$$) {
                                        currentSchema = currentSchema.$$$;
                                        // Send primitive element and the schema for validation:
                                        // Pass the rootItem in order to handle cases
                                        // like passwords equality by referencing other 
                                        // root item's values:
                                        validate(el, currentSchema, true, rootItem);
                                    } else {
                                        // Send the object element and the schema for validation:
                                        // Pass the rootItem in order to handle cases
                                        // like passwords equality by referencing other 
                                        // root item's values:
                                        validate(el, currentSchema, false, rootItem);
                                    }
                                    // Add the schema to the content's array:
                                    schema[k].content.push(currentSchema);
                                });
                            // End of forEach
                        }
                        break;
                    // if end
                }
                // Switch end
            });
        // Object.keys end
    }
    // if end
};

export const isValid = (schema, validity = [], itemIsPrimitiveArrayElement) => {
    // If PRIMITIVE:
    if (itemIsPrimitiveArrayElement) {
        if (schema.errors) {
            validity.push(...schema.errors);
        } else {
            validity.push(schema.errors);
        }
        // If ARRAY or OBJECT:    
    } else {
        Object.keys(schema)
            .forEach(k => {
                // Get own errors:
                if (schema[k].errors) {
                    validity.push(...schema[k].errors);
                } else {
                    validity.push(schema[k].errors);
                }

                // If there is a content:
                // If it's an array get the errors of its elements:
                if (schema[k].content && Array.isArray(schema[k].content)) {
                    schema[k].content
                        .forEach(el => {
                            // If element is a primitive (!!! NB: 'type' key must not be used for a property name)
                            if (el.type && (el.type !== 'object' || el.type !== 'array')) {
                                isValid(el, validity, true);

                                // If element is an array os a plain object:
                            } else {
                                isValid(el, validity);
                            }
                        });
                    // End of nested forEach
                    // If it's an object get the errors of its properties:
                } else if (schema[k].content && typeof schema[k].content === 'object') {
                    isValid(schema[k].content, validity);
                }
            });
        // End of Object.keys .forEach
    }
    return validity.filter(e => e !== null).length === 0;
};
