import {getFullRef} from '../utils';

const required = (val, pred, msg) => {
    const isValid = !!val === pred;
    return isValid ? null : (msg ? msg : 'This field is required');
};

const minLength = (val, min, msg) => {
    const trimmed = Array.isArray(val) ? val : val.trim();
    const isValid = trimmed.length >= min;
    return isValid ? null : (msg ? msg : `Length must be at least ${min}`);
};

const maxLength = (val, max, msg) => {
    const trimmed = Array.isArray(val) ? val : val.trim();
    const isValid = trimmed.length <= max;
    return isValid ? null : (msg ? msg : `Length must be no more than ${max}`);
};

const minValue = (val, min, msg) => {
    const isValid = val >= min;
    return isValid ? null : (msg ? msg : `Value must be at least ${min}`);
};

const maxValue = (val, max, msg) => {
    const isValid = val <= max;
    return isValid ? null : (msg ? msg : `Value must be no more than ${max}`);
};

const type = (val, t, msg) => {
    const isValid = typeof val === t;
    return isValid ? null : (msg ? msg : `Value must be ot type '${t}'`);
};

const pattern = (val, pattern, msg) => {
    const isValid = pattern.test(val);
    return isValid ? null : (msg ? msg : 'Wrong data');
};

// NB: the path won't work on array items, because
// the index is a dynamic property that is not known
// when defining the schema.
const equals = (val, path, msg, rootItem) => {
    const valToTest = getFullRef(rootItem, path);
    // Will work only for primitives:
    //TODO: maybe passa function to check some properties on reference values (eg array lengths etc)
    const isValid = val === valToTest;
    return isValid ? null : (msg ? msg : 'Values are not equal');
};

const validator = {
    equals,
    maxLength,
    maxValue,
    minLength,
    minValue,
    pattern,
    required,
    type
};

export default validator;
