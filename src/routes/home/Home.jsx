import React, {Component} from 'react';

class Home extends Component {

    componentDidMount() {
        console.log('Home component did mount.')
    }

    render() {
        return (
            <h1>Home</h1>
        );
    }

}

export default Home;
