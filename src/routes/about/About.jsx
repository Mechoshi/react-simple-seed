import React, {Component} from 'react';

class About extends Component {

    componentDidMount() {
        console.log('About component did mount.')
    }

    render() {
        return (
            <h1>About</h1>
        );
    }

}

export default About;
