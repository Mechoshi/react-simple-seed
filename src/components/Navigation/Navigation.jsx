import React, {PureComponent} from 'react';
import {NavLink} from 'react-router-dom';
import {connect} from 'react-redux';

class Navigation extends PureComponent {
    render() {
        return (
            <nav>
                <ul>
                    <li><NavLink to="/home">Home</NavLink></li>
                    <li><NavLink to="/about">About</NavLink></li>
                </ul>
            </nav>
        );
    }
}

const mapStateToProps = state => {
    return {};
};

export default connect(mapStateToProps, {})(Navigation);
