import React, {Component} from 'react';

export default (importComponent) => {
    return class extends Component {
        constructor(props) {
            super(props);
            this.state = {component: null};
        }

        async componentDidMount() {
            const cmp = await importComponent();
            this.setState({component: cmp.default});
        }

        render() {
            const C = this.state.component;
            return C ? <C {...this.props} /> : null;
        }
    }
}
