import React, {Component, Fragment} from 'react';
import {BrowserRouter, Redirect, Route, Switch} from 'react-router-dom';
import {connect} from 'react-redux';

import AsyncComponent from './components/AsyncComponent/AsyncComponent';
import Navigation from './components/Navigation/Navigation';

const AsyncHome = AsyncComponent(() => import('./routes/home/Home'));
const AsyncAbout = AsyncComponent(() => import('./routes/about/About'));


class App extends Component {

    componentDidMount() {
        console.log('App component did mount.');
    }

    render() {
        return (
            <BrowserRouter>
                <Fragment>
                    <Navigation/>
                    <Switch>
                        <Route path="/home" component={AsyncHome}/>
                        <Route path="/about" component={AsyncAbout}/>
                        <Redirect from="/" to="/home"/>
                    </Switch>
                </Fragment>
            </BrowserRouter>
        );
    }
}

const mapStateToProps = state => {
    return {}
};

export default connect(mapStateToProps, {})(App);
