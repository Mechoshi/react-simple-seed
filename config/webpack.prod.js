const CompressionPlugin = require('compression-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const path = require('path');
const webpack = require('webpack');
const webpackMerge = require('webpack-merge');

const commonConfig = require('./webpack.common.js');

const ENV = process.env.NODE_ENV || 'production';

/**
 * In order to set API url for development run in the console:
 * npm run develop --apiurl=http://someipaddress:port
 */
const API_URL = (process.env.npm_config_apiurl) || 'http://localhost:8000';

/**
 * Configuration object with properties set for production mode.
 */
const productionSpecificConfig = {
    devtool: 'cheap-module-source-map',
    output:
        {
            path: path.resolve(__dirname, '../dist'),
            publicPath: '/',
            filename: 'public/[name].[chunkhash].js',
            chunkFilename: 'public/[id].chunk.[chunkhash].js'
        },
    plugins: [
        new webpack.NoEmitOnErrorsPlugin(),
        new webpack.optimize.UglifyJsPlugin(),
        new ExtractTextPlugin('public/[name].[chunkhash].css'),
        new OptimizeCssAssetsPlugin(),
        new webpack.DefinePlugin(
            {
                'process.env':
                    {
                        'NODE_ENV': JSON.stringify(ENV),
                        'API_URL': JSON.stringify(API_URL)
                    }
            }
        ),
        new webpack.LoaderOptionsPlugin(
            {
                htmlLoader:
                    {
                        minimize: false
                    }
            }
        ),
        new CompressionPlugin({
            algorithm: 'gzip'
        })
    ]
};

/**
 * Merge the common config with the production specific config.
 */
module.exports = webpackMerge(commonConfig, productionSpecificConfig);
