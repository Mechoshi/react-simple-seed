const autoprefixer = require('autoprefixer');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const path = require('path');
const webpack = require("webpack");

module.exports = {
    entry: {
        app: ['babel-polyfill', './src/main.js'],
        vendor: ['axios', 'react', 'react-dom', 'redux', 'react-redux', 'redux-thunk', 'react-router', 'react-router-dom']
    },
    resolve: {
        extensions: ['.js', '.jsx', '.scss'],
    },
    module: {
        rules: [
            {
                test: /\.jsx?$/,
                loader: 'babel-loader',
                exclude: /node-modules/
            },
            {
                test: /\.html$/,
                exclude: /node-modules/,
                use: 'html-loader'
            },
            {
                test: /\.(woff|woff2|ttf|eot)$/,
                exclude: /node_modules/,
                loader: 'file-loader?name=public/fonts/[name].[ext]'
            },
            {
                test: /\.(png|jpe?g|gif|svg|ico)$/,
                exclude: /node_modules/,
                loader: 'file-loader?name=public/images/[name].[hash].[ext]'
            },
            {
                test: /\.s?css$/,
                exclude: /src/,
                loader: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: [
                        {
                            loader: 'css-loader',
                            options: {
                                importLoaders: 2,
                                sourceMap: true
                            }
                        },
                        {
                            loader: 'postcss-loader',
                            options: {
                                ident: 'postcss',
                                plugins: () => [
                                    autoprefixer({
                                        browsers: ["> 0.1%"]
                                    })
                                ]
                            }
                        },
                        {
                            loader: 'sass-loader'
                        },
                    ]
                })
            }
        ],
    },
    plugins: [
        new webpack.optimize.CommonsChunkPlugin({names: ['vendor', 'manifest']}),
        new webpack.optimize.ModuleConcatenationPlugin(),
        new CopyWebpackPlugin([
            {
                from: `${path.resolve(__dirname, '../')}/favicon.ico`,
                to: `${path.resolve(__dirname, '../dist')}/favicon.ico`
            }
        ]),
        new HtmlWebpackPlugin(
            {
                template: 'src/index.html',
                chunksSortMode: 'dependency',
                favicon: './favicon.ico'
            }
        )
    ]
};
