const express = require('express');
const expressStaticGzip = require("express-static-gzip");
const history = require('connect-history-api-fallback');
const path = require('path');

const PORT = process.env.PORT || 8001;

const app = express();
app.use(history());
app.use(expressStaticGzip(path.resolve(__dirname, '../dist')));

app.listen(PORT, () => {
    console.log(`Production server is running on port ${PORT}.`);
});
